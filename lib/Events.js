const 
    Guild = require('./models/Guild.js'),
    Member = require('./models/Member.js'),
    Channel = require('./models/Channel.js');

module.exports = {
    "READY" : function(data) {
        this.readydata = data;
    },
    
    "GUILD_CREATE" : function(data) {
        data.id = String(data.id);
        this.guilds[data.id] = new Guild({ data: data, mgr : this });
        
        // create channels
        for(let channel of Object.keys(this.guilds[data.id].channels)) {
            this.channels[channel] = this.guilds[data.id].channels[channel];
            this.emit("channel.joined", data);
        }
        
        this.emit("guild.joined", data); 
    },


    "GUILD_BAN_ADD" : function(data) {
        this.guilds[data.guild_id].bans[data.user.id] = data;
        this.emit("guild.banned", data);
    },

    "GUILD_BAN_REMOVE" : function(data) {
        delete this.guilds[data.guild_id].bans[data.user.id];
        this.emit("guild.unbanned", data);
    },

    "GUILD_MEMBER_UPDATE" : function(data) {
        this.guilds[data.guild_id].members[data.user.id] = new Member({ data : data, guild : this.guilds[data.guild_id], mgr : this });
        if(data.user.id === this.id) this.guilds[data.guild_id].me = this.guilds[data.guild_id].members[data.user.id];
    },

    "GUILD_ROLE_UPDATE" : function(data) {
        this.guilds[data.guild_id].roles[data.role.id] = data.role;
    },

    "CHANNEL_CREATE" : function(data) {
        this.channels[data.id] = new Channel({ data : data, mgr : this });
        this.emit("channel.joined", data);
    },

    "CHANNEL_UPDATE" : function(data) {
        for(let key of Object.keys(data)) this.channels[data.id][key] = data[key];
    },
  
    "MESSAGE_CREATE" : function(data) {
        let
            pieces = data.content.split(" "),
            caller = pieces.shift(),
            rest = pieces.join(" ");

        let
            user = (this.db && data.author.id in this.db.users) ? this.db.users[data.author.id] : { id : data.author.id, rank : 0, prefix : this.prefix };

        if(caller.substring(0,1) === user.prefix && 
            caller.substring(1) in this.db.commandMap) {

            let command = this.db.resolveCommand(caller.substring(1));

            if(user.rank >= command.rank) {
                command.exec([rest, this.channels[data.channel_id], user, data, this.channels[data.channel_id].guild], this);
            }

        } 
        
        else {
            this.emit("message", this.channels[data.channel_id], data, user);
        }
    }
};