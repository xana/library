// imports
const
    EventEmitter = require('events').EventEmitter,
    EndpointQueue = require("./models/EndpointQueue.js"),
    WebSocket = require("ws"),
    Request = require("request").defaults({ encoding: null }),
    Events = require("./Events.js"),
    OpEvents = require("./OpEvents.js");

// Symbols for private methods and properties
const
    // methods
    $setupCommands = Symbol(),
    $getEndpoint = Symbol(),
    $dispatch = Symbol(),
    $setupUserData = Symbol(),
    $setupEvents = Symbol(),
    
    // properties
    $avatar = Symbol(),
    $id = Symbol(),
    $discriminator = Symbol(),
    $username = Symbol();

// vars
const
    GW_VERSION = 6,
    GW_ENCODING = 'json',
    VERSION = '0.1.0',
    USER_AGENT = "XanaLibrary (cythral.com v"+VERSION+")";


module.exports = class Library extends EventEmitter {
    
    constructor({ prefix, storage, token, ...rest }) {
        super();
        this.db = storage;
        this.lastSeq = null;
        this.router = new EventEmitter;
        this.token = token;
        this.queue = new EndpointQueue(this.token, rest.userAgent || USER_AGENT);
        this.channels = {};
        this.guilds = {};
        this.prefix = prefix;
        
        this[$setupEvents]();
        this[$getEndpoint]().then(endpoint => this.connect(endpoint));
        this[$setupUserData]();

        process.on('SIGTERM', () => {
            clearInterval(this.heartbeat);
            this.con.close();
            process.exit();
        });
    }

    [$setupEvents]() {
        for(let Event of Object.keys(OpEvents)) { this.router.on(Event, OpEvents[Event].bind(this)); }
        for(let Event of Object.keys(Events)) { this.router.on(Event, Events[Event].bind(this)); }
    }

    /**
     * Retrieves and stores user data about the bot
     */
    async [$setupUserData]() {
        var userData = await this.queue.add({
            endpoint : "https://discordapp.com/api/users/@me",
            method : "GET"
        });

        this[$avatar] = userData.avatar;
        this[$id] = String(userData.id);
        this[$discriminator] = userData.discriminator;
        this[$username] = userData.username;
    }


    /**
     * Retrieves the endpoint
     */
    [$getEndpoint]() {
        return new Promise((resolve, reject) => {
            Request("https://discordapp.com/api/gateway", {
                headers : {
                    "Authorization" : this.token
                }
            }, (err, body, reply) => {
                resolve(JSON.parse(reply).url);
            });
        });
    }

    /**
     * Dispatches events
     */
    [$dispatch](data, flags) {
        data = JSON.parse(data);
        if(data.s !== null) this.lastSeq = data.s;
        
        if(data.op === 0) this.router.emit(data.t, data.d);
        else this.router.emit(data.op, data.d);
    }

    /**
     * Connects to the websocket endpoint
     */
    connect(endpoint) {
        this.con = new WebSocket(endpoint+"?v="+GW_VERSION+"&encoding="+GW_ENCODING);
        this.con.on("open", () => {});
        this.con.on("message", this[$dispatch].bind(this));

        var close = () => {
            this.con.removeAllListeners();
            console.log("disconnected, retrying in 2 seconds");
            setTimeout(() => this.connect(endpoint), 2000);
        };

        this.con.on("close", close);
        this.con.on("error", () => console.log("Error on WS Connection")); 
    }


    /**
     * Resolves a command name or alias to a command
     */
    resolveCommand(command) {
        return (command in this.commandMap) ? this.commands[this.commandMap[command]] : false;
    }



    /** ===================
     * Properties
     */

    get avatar() { return "https://cdn.discordapp.com/avatars/"+this.id+"/"+this[$avatar]+".png?size=2048"; }
    set avatar(value) {
        Request.get(value,  (error, response, body) => {
            if (!error && response.statusCode == 200) {
                var data = "data:" + response.headers["content-type"] + ";base64," + new Buffer(body).toString('base64');
                
                this.queue.add({
                   endpoint : "https://discordapp.com/api/users/@me",
                   data : { avatar : data },
                   method : "PATCH"
                }).then(newData => {
                    this[$avatar] = newData.avatar;
                });                
            }
        });
    }

    get username() { return this[$username]; }
    set username(value) {
        this.queue.add({
           endpoint : "https://discordapp.com/api/users/@me",
           data : { username : value },
           method : "PATCH"
        }).then(newData => {
            this[$username] = newData.username;
            this[$discriminator] = newData.discriminator;
        });
    }

    get discriminator() { return this[$discriminator]; }
    get id() { return this[$id]; }
    get tag() { return this.username+"#"+this.discriminator; }
}