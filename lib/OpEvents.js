module.exports = {
    10 : function(data) {
        this.con.send(JSON.stringify({
            "op" : 2,
            "d" : {
                "token": this.token,
                "properties": {
                    "$os": "linux",
                    "$browser": "mechondiscord",
                    "$device": "mechondiscord",
                    "$referrer": "",
                    "$referring_domain": ""
                },
                "compress": false,
                "large_threshold": 250
            }
        }));

        this.heartbeat = setInterval(() => {
           this.con.send(JSON.stringify({ op : 1, d : this.lastSeq })); 
        }, data.heartbeat_interval);
    }
};